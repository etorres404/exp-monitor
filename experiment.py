#!/usr/bin/env python
#!-*- coding: utf-8 -*-
import argparse
import os
import sys
import multiprocessing
from itertools import repeat, izip, chain

import qstat

class Experiment:
    """
    Experiment class. Stores all the information relative to an experiment.
    """
    def __init__(self, relative_id, inst_folder, solver_path, timeout,
                 inst_extension, qstat):
        # Experiment file info
        self.relative_id = relative_id
        self.inst_folder = inst_folder
        self.solver_path = solver_path
        self.timeout = timeout
        self.inst_extension = inst_extension
        self.qstat = qstat
        # Crafted Info
        self.solver = self._get_simplified_name(self.solver_path)
        self.instances = self._get_simplified_name(self.inst_folder)
        self.total_jobs_names = self._get_job_names()
        self.current_jobs_info, self.current_jobs_names = \
            self._get_current_jobs()
        # Statistics
        self.running = self._get_states(lambda x: x[1] == 'r')
        self.waiting = self._get_states(lambda x: 'w' in x[1])
        self.errors = self._get_states(lambda x: 'E' in x[1])
        self.total = len(self.total_jobs_names)
        self.finished = self.total - self.running - self.waiting - self.errors

    def print_experiment_info(self):
        """Formated information of experiment."""
        # Relative ID
        sys.stdout.write('|' + self._integer_formating(self.relative_id, 4))
        # Instances (simplified)
        sys.stdout.write('|' + self._cut_or_adapt_string(self.instances, 20))
        # Solver (simplified)
        sys.stdout.write('|' + self._cut_or_adapt_string(self.solver, 20))
        # Instances running
        sys.stdout.write('|' + self._integer_formating(self.running, 9))
        # Instances waiting
        sys.stdout.write('|' + self._integer_formating(self.waiting, 9))
        # Instances with errors
        sys.stdout.write('|' + self._integer_formating(self.errors, 7))
        # Finished
        sys.stdout.write('|' + self._integer_formating(self.finished, 10))
        # Total instances
        sys.stdout.write('|' + self._integer_formating(self.total, 7) + '|\n')
        sys.stdout.flush()
    
    def delete(self):
        """Erase all current jobs of the experiment."""
        p = multiprocessing.Pool(8)
        p.map(outsider_delete, self.current_jobs_info)
        
    def all_jobs_finished(self):
        return self.finished == self.total

    def _get_simplified_name(self, name):
        s = name.split('/')
        if s[-1] == '':
            return s[-2]
        else:
            return s[-1]

    def _cut_or_adapt_string(self, string, length):
        return ' {0}{1}'.format(string[:length], ' ' * (length - len(string) - 1))

    def _get_states(self, func):
        # Gets the number of instances that were filtered by function func
        return len(filter(func, self.current_jobs_info))

    def _integer_formating(self, integer, length):
        str_integer = str(integer)
        return ' {0}{1}'.format(str_integer, ' ' * (length - len(str_integer) - 1))

    def __str__(self):
        return ('EXPERIMENT\n'
                'Instance Folder: {0}\n'
                'Solver Path: {1}\n'
                'Timeout: {2}\n'
                'Extension: {3}\n')\
                .format(self.inst_folder, self.solver_path, 
                        self.timeout, self.inst_extension)

    def __repr__(self):
        return str(self)
    
    def _get_instances_gen2(self):
        ext = '.%s' % self.inst_extension
        for path, dir, files in os.walk(self.inst_folder, followlinks=True):
            for f in files:
                if f.endswith(ext):
                    yield os.path.join(path, f)

    def _get_instances_gen(self):
        # Gets the path of experiment instances
        def izr(r, o):
            return izip(repeat(r), o)

        ext = '.%s' % self.inst_extension
        return (os.path.join(r, f) 
                    for r, f in chain.from_iterable(izr(r, files) 
                        for r, _, files in os.walk(self.inst_folder,
                                                   followlinks=True)
                    )
                if f.endswith(ext))

    def _get_job_names(self):
        # Gets all job_names that are submitted by the experiment
        problem = self.instances
        solver = self.solver
        result = set()
        for instance in self._get_instances_gen():
            result.add('log-{0}-{1}-{2}'\
                       .format(solver, problem, instance.replace('/','_')))
        return result        

    def _get_current_jobs(self):
        # Crossing information between qstat result and total jobs
        names_dict = self.qstat[1]
        current_jobs_info = set()
        current_jobs_names = set()
        for job_name in self.total_jobs_names:
            try:
                # Job_name name
                current_jobs_names.add(names_dict[job_name][2])
                # Job_name info
                current_jobs_info.add(names_dict[job_name])
            except KeyError:
                pass
        return current_jobs_info, current_jobs_names


# For parallel deletion of jobs
def outsider_delete(job_id):
    q = qstat.Qstat()
    q.qdel(str(job_id))


