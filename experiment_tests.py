import unittest

import experiment


class ExperimentTestWithAllJobsRunning(unittest.TestCase):
    
    def setUp(self):
        """Setup function to initialize dummy input for Experiment class."""
        # Folder in cluster
        inst_folder = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'instances/random-dummy')
        # Solver path in cluter
        solver_path = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'solvers/r-solver1.py')
        timeout = '50'  # Timeout
        inst_extension = 'cnf'
        self.job_name_1 = ('log-r-solver1.py-random-dummy-_home_eduard_'
                           'Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder A_r-dummy-1.cnf')
        self.job_name_2 = ('log-r-solver1.py-random-dummy-_home_eduard'
                           '_Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder B_r-dummy-2.cnf')

        qstat_id = {'1': ('1', 'r', self.job_name_1, 'usage_1'),
                    '2': ('2', 'r', self.job_name_2, 'usage_2') 
                   }
        qstat_job = {self.job_name_1: ('1', 'r', self.job_name_1, 'usage_1'),
                     self.job_name_2: ('2', 'r', self.job_name_2, 'usage_2')
                    }
        qstat = (qstat_id, qstat_job)  # Qstat result dummy
        self.experiment = experiment.Experiment(inst_folder, solver_path,
                                                timeout, inst_extension, qstat)

    def test_total_jobs(self):
        """Test of total jobs state."""
        self.assertTrue(len(self.experiment.total_jobs_names) == 2)
        self.assertTrue(self.job_name_1 in self.experiment.total_jobs_names)
        self.assertTrue(self.job_name_2 in self.experiment.total_jobs_names)

    def test_current_jobs(self):
        """Test of current jobs state."""
        self.assertTrue(len(self.experiment.current_jobs_info) == 2)
        self.assertTrue(self.job_name_1 in self.experiment.current_jobs_names)
        self.assertTrue(self.job_name_2 in self.experiment.current_jobs_names)

    def test_running_jobs(self):
        """Test of number of running jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: x[1] == 'r'), 2)

    def test_queue_jobs(self):
        """Test of number of queue jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'w' in x[1]), 0)

    def test_error_jobs(self):
        """Test of number of error jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'E' in x[1]), 0)


class ExperimentTestWithOneTestRunning(unittest.TestCase):
    def setUp(self):
        """Setup function to initialize dummy input for Experiment class."""
        # Folder in cluster
        inst_folder = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'instances/random-dummy')
        # Solver path in cluter
        solver_path = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'solvers/r-solver1.py')
        timeout = '50'  # Timeout
        inst_extension = 'cnf'
        self.job_name_1 = ('log-r-solver1.py-random-dummy-_home_eduard_'
                           'Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder A_r-dummy-1.cnf')
        self.job_name_2 = ('log-r-solver1.py-random-dummy-_home_eduard'
                           '_Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder B_r-dummy-2.cnf')

        qstat_id = {'1': ('1', 'r', self.job_name_1, 'usage_1')}
        qstat_job = {self.job_name_1: ('1', 'r', self.job_name_1, 'usage_1')}
        qstat = (qstat_id, qstat_job)  # Qstat result dummy
        self.experiment = experiment.Experiment(inst_folder, solver_path,
                                                timeout, inst_extension, qstat)

    def test_total_jobs(self):
        """Test of total jobs state."""
        self.assertTrue(len(self.experiment.total_jobs_names) == 2)
        self.assertTrue(self.job_name_1 in self.experiment.total_jobs_names)
        self.assertTrue(self.job_name_2 in self.experiment.total_jobs_names)

    def test_current_jobs(self):
        """Test of current jobs state."""
        self.assertTrue(len(self.experiment.current_jobs_info) == 1)
        self.assertTrue(self.job_name_1 in self.experiment.current_jobs_names)

    def test_running_jobs(self):
        """Test of number of running jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: x[1] == 'r'), 1)

    def test_queue_jobs(self):
        """Test of number of queue jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'w' in x[1]), 0)

    def test_error_jobs(self):
        """Test of number of error jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'E' in x[1]), 0)


class ExperimentTestWithTwoInstancesInQueue(unittest.TestCase):
    def setUp(self):
        """Setup function to initialize dummy input for Experiment class."""
        # Folder in cluster
        inst_folder = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'instances/random-dummy')
        # Solver path in cluter
        solver_path = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'solvers/r-solver1.py')
        timeout = '50'  # Timeout
        inst_extension = 'cnf'
        self.job_name_1 = ('log-r-solver1.py-random-dummy-_home_eduard_'
                           'Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder A_r-dummy-1.cnf')
        self.job_name_2 = ('log-r-solver1.py-random-dummy-_home_eduard'
                           '_Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder B_r-dummy-2.cnf')

        qstat_id = {'1': ('1', 'qw', self.job_name_1, 'usage_1'),
                    '2': ('2', 'qw', self.job_name_2, 'usage_2')
                   }
        qstat_job = {self.job_name_1: ('1', 'qw', self.job_name_1, 'usage_1'),
                     self.job_name_2: ('2', 'qw', self.job_name_2, 'usage_2')
                    }
        qstat = (qstat_id, qstat_job)  # Qstat result dummy
        self.experiment = experiment.Experiment(inst_folder, solver_path,
                                                timeout, inst_extension, qstat)

    def test_running_jobs(self):
        """Test of number of running jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: x[1] == 'r'), 0)

    def test_queue_jobs(self):
        """Test of number of queue jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'w' in x[1]), 2)

    def test_error_jobs(self):
        """Test of number of error jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'E' in x[1]), 0)


class ExperimentTestWithTwoInstancesWithErrors(unittest.TestCase):
    def setUp(self):
        """Setup function to initialize dummy input for Experiment class."""
        # Folder in cluster
        inst_folder = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'instances/random-dummy')
        # Solver path in cluter
        solver_path = ('/home/eduard/Documentos/IA/exp-monitor/dummy/'
                      'solvers/r-solver1.py')
        timeout = '50'  # Timeout
        inst_extension = 'cnf'
        self.job_name_1 = ('log-r-solver1.py-random-dummy-_home_eduard_'
                           'Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder A_r-dummy-1.cnf')
        self.job_name_2 = ('log-r-solver1.py-random-dummy-_home_eduard'
                           '_Documentos_IA_'
                           'exp-monitor_dummy_instances_random-dummy_'
                           'folder B_r-dummy-2.cnf')

        qstat_id = {'1': ('1', 'E', self.job_name_1, 'usage_1'),
                    '2': ('2', 'E', self.job_name_2, 'usage_2')
                   }
        qstat_job = {self.job_name_1: ('1', 'E', self.job_name_1, 'usage_1'),
                     self.job_name_2: ('2', 'E', self.job_name_2, 'usage_2')
                    }
        qstat = (qstat_id, qstat_job)  # Qstat result dummy
        self.experiment = experiment.Experiment(inst_folder, solver_path,
                                                timeout, inst_extension, qstat)

    def test_running_jobs(self):
        """Test of number of running jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: x[1] == 'r'), 0)

    def test_queue_jobs(self):
        """Test of number of queue jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'w' in x[1]), 0)

    def test_error_jobs(self):
        """Test of number of error jobs."""
        self.assertEquals(self.experiment._get_states(lambda x: 'E' in x[1]), 2)


if __name__ == '__main__':
    unittest.main()

