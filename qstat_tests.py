import unittest

import qstat


class QstatDesktopTests(unittest.TestCase):
    
    def test_parse_qstat(self):
        q = qstat.Qstat()
        with open('/home/eduard/Documentos/IA/exp-monitor/dummy2/qstat_out.txt') as stream:
            r = stream.readlines()
        crafted_res = {'1388787': ['1388787', 'r'],
                       '1388815': ['1388815', 'r'],
                       '1388788': ['1388788', 'r']}
        res = q._parse_qstat(r)
        self.assertEquals(res, crafted_res)


if __name__ == '__main__':
    unittest.main()

