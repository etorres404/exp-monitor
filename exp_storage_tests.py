import unittest

import exp_storage

# Class qstat dummy
class QstatDummy:
    def __init__(self):
        pass

    def qstat(self):
        # 4 of 5 experiments executing
        return {'1': ['1', 'r'],
                '2': ['2', 'r'],
                '10': ['10', 'E'],
                '5': ['5', 'qw']}

    def qstat_job(self, job):
        # Experiment 1 dummy info
        job_name_1_1 = ('log-c-solver1.py-crafted-dummy-_home_eduard_'
                         'Documentos_IA_'
                         'exp-monitor_dummy_instances_crafted-dummy_'
                         'folder 1_c-dummy-1.cnf')
        job_name_1_2 = ('log-c-solver1.py-crafted-dummy-_home_eduard_'
                        'Documentos_IA_'
                        'exp-monitor_dummy_instances_crafted-dummy_'
                        'folder 2_c-dummy-2.cnf')
        # Not used, job finished
        job_name_1_3 = ('log-c-solver1.py-crafted-dummy-_home_eduard_'
                        'Documentos_IA_'
                        'exp-monitor_dummy_instances_crafted-dummy_'
                        'folder 2_c-dummy-3.cnf')

        # Experiment 2 dummy info
        job_name_2_1 = ('log-r-solver1.py-random-dummy-_home_eduard_'
                        'Documentos_IA_'
                        'exp-monitor_dummy_instances_random-dummy_'
                        'folder A_r-dummy-1.cnf')
        job_name_2_2 = ('log-r-solver1.py-random-dummy-_home_eduard'
                        '_Documentos_IA_'
                        'exp-monitor_dummy_instances_random-dummy_'
                        'folder B_r-dummy-2.cnf')
        d = {'1': [job_name_2_1, 'usage_1'],
             '2': [job_name_2_2, 'usage_2'],
             '10': [job_name_1_1, 'usage_10'],
             '5': [job_name_1_2, 'usage_5']}
        return d[job]


class TestExpStorageMethods(unittest.TestCase):
    
    def test_output_format(self):
        e = exp_storage.ExpStorage('/home/eduard/Documentos/IA/exp-monitor/dummy/exp/dummy-exp.txt',
                                   q_class=QstatDummy)
        e.experiment_info()

if __name__ == '__main__':
    unittest.main()

