#!/usr/bin/env python
#!-*- coding: utf-8 -*-
import subprocess
import re
import shlex
import sys
import os


class Qstat:
    """Qstat query class."""
    
    # Regular expression to parse job_name
    global JOB_NAME_EXPR
    JOB_NAME_EXPR = re.compile(r'job_name:\s*([\s\S]+)')

    # Regular expression to parse usage (needs testing)
    global USAGE_EXPR
    USAGE_EXPR = re.compile(r'usage\s+1:\s*([\s\S]+)')

    def __init__(self):
        pass

    def qstat(self):
        """Execution and parsing of qstat command."""
        res = self._execute_command('qstat')
        return self._parse_qstat(res.split('\n'))

    def _parse_qstat(self, qstat_result):
        result_dict = dict()
        qstat_result = qstat_result[2:]
        for line in (l.split() for l in qstat_result):
            if line != []:
                result_dict[line[0]] = [line[0], line[4], line[7]]
        return result_dict 

    def qstat_job(self, job):
        """Execution of qstat with job id."""
        res = self._execute_command('qstat -j %s' % job)
        return self._parse_qstat_job(res)

    def _parse_qstat_job(self, res):
        job_name = None
        usage = None
        for l in res.split('\n'):
            if not job_name:
                job_name = self._regex_parse(l, JOB_NAME_EXPR)
            if not usage:
                usage = self._regex_parse(l, USAGE_EXPR) 
        return [job_name, usage]

    def _regex_parse(self, l, expression):
        m = expression.match(l)
        if m:
            return m.group(1)
        return None

    def qdel(self, id):
        """Deletion of job with id <id>."""
        self._execute_command('qdel %s' % id)

    def _execute_command(self, command):
        """Generic execution of command."""
        FNULL = open(os.devnull, 'w')
        proc = subprocess.Popen(shlex.split(command),
                                stdout=subprocess.PIPE, stderr=FNULL)       
        out, err = proc.communicate()
        proc.wait()
        return out


if __name__ == '__main__':
    q = Qstat()
    print q.qstat()
    print ''
    print q.qdel(sys.argv[1])
    print q.qstat()

