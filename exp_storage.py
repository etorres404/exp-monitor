#!/usr/bin/env python
#!-*- coding: utf-8 -*-
import argparse
import readline
import sys
import multiprocessing
import itertools

import qstat  # qstat queries
import experiment


class ExpStorage:
    """Stores all experiments of a experiment file."""
    def __init__(self, exp_filename, q_class=qstat.Qstat):
        self.q_c = q_class()
        self.experiments, self.exp_dict = self._create_experiments(exp_filename)

    def experiment_info(self):
        """Prints basic information of every experiment."""
        # Format declaration
        separator = '---------------------------------------------' \
                    '--------------------------------------------------'
        header = ('| ID |      INSTANCES     |        SOLVER      | RUNNING |'
                  ' WAITING | ERROR | FINISHED | TOTAL |')
        # Print information
        print separator
        print header
        print separator
        print separator
        for exp in self.experiments:
            exp.print_experiment_info()
            print separator

    def deletion_mode(self):
        """Enters on deletion mode, where the user can delete experiments."""
        # First print experiment info
        self.experiment_info()
        # Enters on interactive mode
        print '>> Enter the number of experiment to be erased'
        print '   or a sequence of numbers separated by space'
        readline.parse_and_bind('tab: complete')
        n_experiment = raw_input()
        exps = self._process_experiments(map(int, n_experiment.split()))
        if exps:  # If there are no experiments, no need to do nothing more
            exps_str = ','.join(map(str, exps))
            print '>> Are you sure you want to delete experiment %s? (y/n)' % exps_str
            response = raw_input()
            if response == 'n':
                print '>> Deletion of experiment %s aborted' % exps_str
            else:
                for e in exps:
                    self.exp_dict[e].delete()
                    print '>> Experiment %d deleted' % e 
        
    def _process_experiments(self, exp):
        # Analysis of which introduced experiments are valid
        result = []
        for e in exp:
            if e < 1 or e > len(self.exp_dict):
                print '>> %d is not a valid experiment' % e
                continue
            if self.exp_dict[e].all_jobs_finished():
                print '>> All jobs in experiment %d are already finished' % e
                continue
            result.append(e)
        return result

    def _create_experiments(self, filename):
        experiments = []  # For monitorization
        exp_dict = {}  # For deletion
        exp_file_data = self._collect_file_data(filename)
        qstat_result = self._execute_qstat()
        for relative_id, info in enumerate(exp_file_data, start=1): 
            exp = experiment.Experiment(relative_id=relative_id,
                                        inst_folder=info[0],
                                        solver_path=info[1],
                                        timeout=info[2],
                                        inst_extension=info[3],
                                        qstat=qstat_result)
            experiments.append(exp)
            exp_dict[relative_id] = exp
        return experiments, exp_dict

    def _execute_qstat(self):
        # All jobs executing now. q_class returns parsed results
        return self._complete_job_info(self.q_c.qstat())

    def _complete_job_info(self, jobs):
        # Complete job info from jobs. From dict of q_class, returns tuple
        # with two dicts of tuple (job_id, state, job_name, usage), one
        # with job_id as key and the other with job_names
        job_names_dict = dict()
        p = multiprocessing.Pool(8)
        info = p.map(outsider_seq_job_info, zip(itertools.repeat(jobs),
                                                itertools.repeat(job_names_dict),
                                                jobs.keys()))
        for i in info:
            job_names_dict[i[3]] = i
            jobs[i[1]] = i

        return jobs, job_names_dict
    
    @staticmethod
    def seq_job_info((jobs, job_names_dict, key)):
        q = qstat.Qstat()
        # Result of command qstat -j with `key` id
        job_info = q.qstat_job(key)
        # Previous info obtained with qstat command
        prev_info = jobs[key]
        # Extends previous info with new info
        prev_info.extend(job_info)
        return tuple(prev_info)

    def _collect_file_data(self, filename):
        """Collects all experiments in filename."""
        with open(filename, 'r') as stream:
            experiments = []
            for line in stream:
                if line[0] == '#':
                    continue
                l = line.split()
                experiments.append((l[0], l[1], l[2], l[3]))
            return experiments


# Multiprocessing pool only works with non-class functions
def outsider_seq_job_info(tupl):
    return ExpStorage.seq_job_info(tupl)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Experiment monitor.')
    parser.add_argument('--delete', '-d', action='store_true',
                        required=False, default=False,
                        help='Enter on experiment deletion mode')
    parser.add_argument('filename', type=str, help='Filename of experiment file')
    args = parser.parse_args()
    exp_storage = ExpStorage(args.filename)
    if args.delete:
        exp_storage.deletion_mode()
    else:
        exp_storage.experiment_info()

